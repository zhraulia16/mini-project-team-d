const router = require("express").Router();

const parentRouter = require('./parentRouter');
const teacherRouter = require('./teacherRouter');


const controller = require('../controllers/index')
const order = require('../controllers/orderController')
// const teacher = require('../controllers/teacherController')

router.use('/parent', parentRouter);
router.use('/teacher', teacherRouter)

router.get('/', (req, res) => res.render('index'))
router.get('/modal', (req, res) => res.render('dashboard_parent'))

router.get('/register', (req, res) => res.render('register'))
router.post('/register', controller.register)

router.post('/createOrder', order.createOrder)

// router.get('/api-teacher', teacher.allTeacherAPI);
// router.get('/api-teacher-lokasi/:lokasi', teacher.allTeacherAPI);
// router.get('/api-teacher-mapel/:mapel', teacher.allTeacherAPI);
// router.get('/api-teacher/:mapel/:lokasi', teacher.allTeacherAPI);

// router.get('/api-teacher-harga/', teacher.allTeacherHargaAPI);
// router.get('/api-teacher-harga/:maks', teacher.allTeacherHargaAPI);
// router.get('/api-teacher-harga/:from/:to', teacher.allTeacherHargaAPI);

// router.get('/api-teacher-rate', teacher.allTeacherRateAPI);
// router.get('/api-teacher-rate/:id', teacher.getRatingAPI);

module.exports = router;