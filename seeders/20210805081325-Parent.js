'use strict';

const bcrypt = require("bcrypt");
const ROLE = {
  PARENT: 'parent',
  TEACHER: 'teacher'
}

module.exports = {
  up: async (queryInterface, Sequelize) => {

    await queryInterface.bulkInsert('Parents', [{
      nama: "Dyah",
      email: "dyah@gmail.com",
      password: bcrypt.hashSync("11111", 10),
      avatar: "https://reqres.in/img/faces/7-image.jpg",
      no_telfon: "https://wa.wizard.id/d33c4e",
      role: ROLE.PARENT,
      provinsi: "Jawa Barat",
      kabupaten: "Kota Bogor",
      kecamatan: "Bogor Tengah",
      kelurahan: "Paledang",
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      nama: "Suprapto",
      email: "suprapto@gmail.com",
      password: bcrypt.hashSync("11111", 10),
      avatar: "https://reqres.in/img/faces/4-image.jpg",
      no_telfon: "https://wa.wizard.id/d33c4e",
      role: ROLE.PARENT,
      provinsi: "Jawa Barat",
      kabupaten: "Kota Bogor",
      kecamatan: "Tanah Sereal",
      kelurahan: "Kencana",
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});

  },

  down: async (queryInterface, Sequelize) => {

    await queryInterface.bulkDelete('Parents', null, {});

  }
};