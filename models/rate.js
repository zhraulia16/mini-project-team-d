'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Rate extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Rate.belongsTo(models.Teacher)
      Rate.belongsTo(models.Parent)
    }
  };
  Rate.init({
    TeacherId: DataTypes.INTEGER,
    ParentId: DataTypes.INTEGER,
    isFinished: DataTypes.BOOLEAN,
    rate: DataTypes.INTEGER,
    comment: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'Rate',
  });
  return Rate;
};