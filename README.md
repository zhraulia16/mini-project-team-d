# Mini Proyek Kelompok D: GuruKu
## Deskripsi
Pada mini proyek ini tim kami akan membuat sebuah produk berbasis web yang bertujuan untuk memudahkan para orang tua untuk mencari guru privat terdekat.   
## Cara Instalasi
   1. Clone this repository to your local computer
   2. Install all dependencies

   ```markdown
   npm install
   ```

   3. Create database

   ```markdown
   sequelize db:create
   ```

   4. Migrate database schema

   ```markdown
   sequelize db:migrate
   ```

   5. Seed data dummy

   ```markdown
   sequelize db:seed:all
   ```

   6. Run this application

   ```markdown
   npm start
   ```
## Cara Penggunaan 

   1. Register
   Alur kerjanya:
   - Input data dari form;
   - Filter data yang diinputkan;
   - Simpan ke database;
   - Kalau berhasil, alihkan ke halaman login.

   2. Login
   - Open website GuruKu
   - Klik CTA 'Temukan Guru'. jika sudah memiliki akun akan ada pilihan role teacher atau parent lalu pilih 1 diantara keduanya. 

   3. Role Parent
   parent's dashboard terdiri dari 4 page : 
   - page Explorasi 
      * isi mata pelajaran dan kota/kabupaten guru yang ingin di cari --> pilih guru privat --> klik card "guru privat" --> setelah itu akan otomatis masuk ke detail guruku --> selanjutnya klik booking sekarang--> ada 3 pilihan yaitu jika klik tutup pop up akan kembali ke page "detail guruku" jika klik "kontak guru" maka akan diarahkan ke whatsapp guru jika klik "kembali ke dashboard maka akan masuk page "Dashboard".
   - page kegiatan
      * klik link Spreadsheet --> menampilkan halaman rapot
   - page akun
      * edit profil --> - edit data diri - edit foto profil - simpan perubahan - batalkan perubahan
   - page keluar
      * Klik keluar --  > muncul "Yakin ingin keluar ?" --> jika ya klik keluar maka akan keluar dari akun jika tidak klik batal dan akan kembali ke page dashboard.

   4. Role Teacher
   Teacher's dashboard terdiri dari 5 page :
   - page ulasan
      * klik ulasan --> muncul ulasan orangtua
   - page kegiatan
      * klik kegiatan--> jika memiliki murid maka akan masuk ke page kegiatan jika tidak maka akan kembali ke dashboard --> telah masuk ke page kegiatan--> klik tambah kegiatan--> ada 3 pilihan untuk pilihan pertama tutup pop up maka akan kembali ke page kegiatan, pilihan kedua ialah pilih card --> klik link spreadsheet --> menampilkan halaman rapot, yang ketiga isi detail kegiatan belajar jika mengklik option tersebut akan muncul 2 pilihan , ''batal'' akan mengembalikan ke halaman page kegiatan ''buat'' kegiatan berhasil ditambahkan
   - page dashboard
      * Klik card --> masuk page "Detail Orang Tua" --> klik "Kontak Sekarang" --> diarahkan ke Whatsapp orang tua. 
   - page profil
      * klik profil --> ada 2 fitur ; 1 fitur edit profil pada edit profil ada 4 aktifitas yaitu - Edit foto profil - edit data diri - batalkan perubahan - simpan perubahan 2 fitur iklan ada 2 aktifitas - tutup pop up akan mengembalikan pada halaman page iklan - masuk pop up edit iklan --> akan diarahkan mengisi detail iklan jika sudah selesai mengisi bisa dibatalkan atau disimpan. 

   - page keluar
      * Klik keluar --> muncul "Yakin ingin keluar ?" --> jika ya klik keluar maka akan keluar dari akun jika tidak klik batal dan akan kembali ke page dashboard.

   
## Fitur-fitur yang ada pada Web Guruku

   1. Register : 
      Fitur ini digunakan untuk membuat akun
   2. Login :
      Fitur ini digunakan untuk masuk ke dalam web dengan memasukkan      email dan password.
   3. Dashboard :
      Merupakan halaman depan website yang berisi informasi aktivitas terkini website dan akses cepat pembuatan posting baru.
   4. Explorasi :
      - Fitur ini berisi tampilan daftar guru yang akan di Booking
   5. Fitur Search di halaman eksplorasi : 
      Fitur ini digunakan untuk mencari guru sesuai 
   6. kegiatan :
      Fitur ini digunakan untuk ;
      - Guru menulis kegiatan
      - Guru mengubah kegiatan
      - Guru menghapus kegiatan
      - Orang tua melihat kegiatann anak 
   7. Ulasan :
      Orang tua memberikan rating dan menulis ulasan setelah mengakhiri kerjasama
   8. Profil :
       Fitur ini digunakan untuk melihat dan mengelola profile pengguna
   9. pesan guru :
      Fitur ini digunakan untuk ;
      - Memilih teacher
      - Konfimasi teacher
      - Mengakhiri kerjasama
   10. Fitur edit akun :
      Orang tua dan guru dapat melihat dan mengubah pengaturan akun
   11. Log out :
      Fitur ini digunakan untuk keluar dari website.


 
 
   

