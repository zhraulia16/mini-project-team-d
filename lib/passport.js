const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const bcrypt = require("bcrypt");

const { Teacher, Parent } = require('../models')

async function authenticateTeacher(email, password, done){
    try {
        // Memanggil method kita tadi
        const teacher = await Teacher.authenticate({ email, password })

        return done(null, teacher)
    } catch (err) {
        return done(null, false, { message: err })
    }
}

async function authenticateParent(email, password, done){
    try {
        // Memanggil method kita tadi
        const parent = await Parent.authenticate({ email, password })

        return done(null, parent)
    } catch (err) {
        return done(null, false, { message: err })
    }
}

passport.use('teacher',
    new LocalStrategy({
            usernameField: "email",
            passwordField: "password",
        },
        authenticateTeacher
    )
);

passport.use('parent',
    new LocalStrategy({
            usernameField: "email",
            passwordField: "password",
        },
        authenticateParent   
    )
);


passport.serializeUser((teacher, done) => {
    done(null, teacher);
});
passport.deserializeUser((obj, done) => {
    done(null, obj);
});

module.exports = passport
