const ROLE = {
  ADMIN: 'admin',
  PARENT: 'parent',
  TEACHER: 'teacher'
}

class Middleware{
    static restrictParent(req, res, next) {
        if (req.isAuthenticated()) return next();
          
        res.redirect("/parent/login");    
    };

    static restrictTeacher(req, res, next) {
      if (req.isAuthenticated()) return next();
        
      res.redirect("/teacher/login");    
    };
    
    static checkRole (req, res, next) { 
        if (req.user.role === ROLE.TEACHER && req.user.role != undefined) {
           res.redirect("/teacher")
        } else if(req.user.role === ROLE.PARENT && req.user.role != undefined){
          res.redirect("/parent");
        }

        return next()
    };
}

module.exports = Middleware;