$(':radio').change(function () {
    // console.log('New star rating: ' + this.value);
});

$(document).ready(function () {
    function validateComment() {
        let comValue = $('#comment').val();
        if (comValue == '') {
            $('#comcheck').html('Ulasan anda masih kosong');
            return false
        } else {
            $('#comcheck').html('');
            return true;
        }
    }

    function validateRate() {
        var isValid = $("input[name=rate]").is(":checked");

        if (!isValid) {
            $('#ratecheck').html('Wajib Kasih Penilaian');
            return false
        } else {
            $('#ratecheck').html('');
            return true
        }
    }

    $("#add-rating").click(e => {
        e.preventDefault()
        let valid_rate = validateRate();
        let valid_comment = validateComment();
        if ((valid_rate == true) &&
            (valid_comment == true)) {
            return true;
        } else {
            return false;
        }
    });
});


// API section
//select elements
const provinsiListElement = document.getElementById("provinsi-list")
const kabupatenListElement = document.getElementById("kabupaten-list")
const kecamatanListElement = document.getElementById("kecamatan-list")
const kelurahanListElement = document.getElementById("kelurahan-list")

function resetSelectElement(selectElement) {
    selectElement.selectedIndex = 0; // first option is selected
    for (i = selectElement.options.length - 1; i >= 1; i--) {
        selectElement.remove(i);
    }
}

const kelurahan = (kecamatanId) => {
    const index = new XMLHttpRequest();
    index.onload = function () {
        const responseText = this.responseText
        const response = JSON.parse(responseText)


        for (const kelurahan of response.kelurahan) {
            const option = document.createElement("option")
            option.classList.add("kelurahan-option")
            option.setAttribute("value", kelurahan.nama)
            option.setAttribute("name", kelurahan.id)
            option.innerText = kelurahan.nama

            kelurahanListElement.append(option)
        }

        kelurahanListElement.addEventListener("change", function () {
            let namaKelurahan = kelurahanListElement.value;
            document.getElementById("destination-kelurahan").value = namaKelurahan
        })

    }
    index.open("GET",
        `https://dev.farizdotid.com/api/daerahindonesia/kelurahan?id_kecamatan=${kecamatanId}`
    );
    index.send();
}

const kecamatan = (kabupatenId) => {
    const index = new XMLHttpRequest();
    index.onload = function () {
        const responseText = this.responseText
        const response = JSON.parse(responseText)

        for (const kecamatan of response.kecamatan) {
            const option = document.createElement("option")
            option.classList.add("kecamatan-option")
            option.setAttribute("value", kecamatan.nama)
            option.setAttribute("name", kecamatan.id)
            option.innerText = kecamatan.nama

            kecamatanListElement.append(option)
        }

        kecamatanListElement.addEventListener("change", function () {
            resetSelectElement(kelurahanListElement)
            let namaKecamatan = kecamatanListElement.value;
            document.getElementById("destination-kecamatan").value = namaKecamatan

            for (const kecamatan of response.kecamatan) {
                if (kecamatan.nama === namaKecamatan) {
                    kelurahanListElement.removeAttribute("disabled")
                    kelurahan(kecamatan.id)
                }
            }
        })

    }
    index.open("GET",
        `https://dev.farizdotid.com/api/daerahindonesia/kecamatan?id_kota=${kabupatenId}`
    );
    index.send();
}


const kota_kabupaten = (provinsiId) => {
    const index = new XMLHttpRequest();
    index.onload = function () {
        const responseText = this.responseText
        const response = JSON.parse(responseText)

        for (const kabupaten of response.kota_kabupaten) {
            const option = document.createElement("option")
            option.classList.add("kabupaten-option")
            option.setAttribute("value", kabupaten.nama)
            option.setAttribute("name", kabupaten.id)
            option.innerText = kabupaten.nama

            kabupatenListElement.append(option)
        }

        kabupatenListElement.addEventListener("change", function () {

            resetSelectElement(kecamatanListElement)
            resetSelectElement(kelurahanListElement)


            let namaKabupaten = kabupatenListElement.value;
            document.getElementById("destination-kabupaten").value = namaKabupaten

            for (const kabupaten of response.kota_kabupaten) {
                if (kabupaten.nama === namaKabupaten) {
                    kecamatanListElement.removeAttribute("disabled")
                    kecamatan(kabupaten.id)
                }
            }
        })

    }
    index.open("GET",
        `https://dev.farizdotid.com/api/daerahindonesia/kota?id_provinsi=${provinsiId}`
    );
    index.send();
}

const provinsi = () => {
    const index = new XMLHttpRequest();
    index.onload = function () {
        const responseText = this.responseText
        const response = JSON.parse(responseText)


        for (const provinsi of response.provinsi) {
            const optionElement = document.createElement("option")
            optionElement.setAttribute("value", provinsi.nama)
            optionElement.innerText = provinsi.nama

            provinsiListElement.append(optionElement)
        }

        provinsiListElement.addEventListener("change", function () {

            resetSelectElement(kabupatenListElement)
            resetSelectElement(kecamatanListElement)
            resetSelectElement(kelurahanListElement)


            let namaProvonsi = provinsiListElement.value
            document.getElementById("destination-provinsi").value = namaProvonsi

            kabupatenListElement.removeAttribute("disabled")
            for (const provinsi of response.provinsi) {
                if (provinsi.nama === namaProvonsi) {
                    kabupatenListElement.removeAttribute("disabled")
                    kota_kabupaten(provinsi.id)
                }
            }

        })

    }
    index.open("GET",
        "https://dev.farizdotid.com/api/daerahindonesia/provinsi"
    );
    index.send();
}

provinsi()