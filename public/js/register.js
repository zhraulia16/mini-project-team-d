$(document).ready(function () {
    function validateNama(){
        let namaValue = $('#nama').val();
        if(namaValue == ''){
            $('#namacheck').html('nama masih kosong');
            return false   
        }else{
            $('#namacheck').html('');
            return true;
        }
    }

    function ValidateRegex(mail){
        let regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        if (regex.test(mail))
        {
            return (true)
        }
        return (false)
    }
    function validateEmail(){
        let emailValue = $('#email').val();
        if(emailValue == ''){
            $('#emailvalid').html('email masih kosong');
            return false;
        }else if(!ValidateRegex(emailValue)){
            $('#emailvalid').html('email tidak valid');
            return false;
        }else{
            $('#emailvalid').html('');
            return true;
        }
    }

    function validateAlamat(){
        let namaValue = $('#alamat').val();
        if(namaValue == ''){
            $('#alamatcheck').html('alamat masih kosong');
            return false   
        }else{
            $('#alamatcheck').html('');
            return true;
        }
    }

    function validatePassword(){
      let passValue = $('#password').val();
      if(passValue == ''){
          $('#passcheck').html('password masih kosong');
          return false;
      }else if(passValue.length < 5){
          $('#passcheck').html('panjang password minimal 5 karakter');
          return false;
      }else{
          $('#passcheck').html('');
          return true;
      }
    }

    function validateConfirmPassword(){
        let passValue = $('#password').val();
        let con_passValue = $('#conpassword').val();
        if(passValue != con_passValue || passValue == ''){
            $('#conpasscheck').html('password tidak sama');
            return false;
        }else{
            $('#conpasscheck').html('');
            return true;
        }
    }

    function validateRadioRole(){
        var isValid = $("input[name=role]").is(":checked");
        
        if(!isValid){
            $('#rolecheck').html('Role Wajib Dipilih');
            return false
        }else{
            $('#rolecheck').html('');
            return true
        }
    }

    function validateProvinsi(){
        let provValue = $('#destination-provinsi').val();
        if(provValue == ''){
            $('#provcheck').html('domisili(provinsi) masih kosong');
            return false   
        }else{
            $('#provcheck').html('');
            return true;
        }
    }

    function validateKabupaten(){
        let kabValue = $('#destination-kabupaten').val();
        if(kabValue == ''){
            $('#kabcheck').html('domisili(kabupaten) masih kosong');
            return false   
        }else{
            $('#kabcheck').html('');
            return true;
        }
    }

    function validateKecamatan(){
        let kecValue = $('#destination-kecamatan').val();
        if(kecValue == ''){
            $('#keccheck').html('domisili(kecamatan) masih kosong');
            return false   
        }else{
            $('#keccheck').html('');
            return true;
        }
    }

    function validateKelurahan(){
        let kelValue = $('#destination-kelurahan').val();
        if(kelValue == ''){
            $('#kelcheck').html('domisili(kelurahan) masih kosong');
            return false   
        }else{
            $('#kelcheck').html('');
            return true;
        }
    }

    function validateEndCheck(){
        var isEndValid = $("input[name=centang]").is(":checked");
        
        if(!isEndValid){
            $('#endcheck').html('Persetujuan ini wajib dicentang');
            return false
        }else{
            $('#endcheck').html('');
            return true
        }
    }

    // Submitt button
    $('#submitbtn').click(function () {
        let valid_nama = validateNama();
        let valid_email = validateEmail();
        let valid_pass = validatePassword();
        let valid_con_pass = validateConfirmPassword();
        let valid_radio = validateRadioRole();
        let valid_prov = validateProvinsi();
        let valid_kab = validateKabupaten();
        let valid_kec = validateKecamatan();
        let valid_kel = validateKelurahan();
        let valid_end = validateEndCheck();

        if( (valid_nama == true) && 
            (valid_email == true) && 
            (valid_pass == true) && 
            (valid_con_pass == true) && 
            (valid_radio == true) && 
            (valid_prov == true) &&
            (valid_kab == true) &&
            (valid_kec == true) &&
            (valid_kel == true) &&
            (valid_end == true) ){
            return true;
        }else{
            return false;
        }
    });

});

//========================

//select elements
const provinsiListElement = document.getElementById("provinsi-list")
const kabupatenListElement = document.getElementById("kabupaten-list")
const kecamatanListElement = document.getElementById("kecamatan-list")
const kelurahanListElement = document.getElementById("kelurahan-list")

function resetSelectElement(selectElement) {
    selectElement.selectedIndex = 0; // first option is selected
    for (i = selectElement.options.length - 1; i >= 1; i--) {
        selectElement.remove(i);
    }
}

const kelurahan = (kecamatanId) => {
    const index = new XMLHttpRequest();
    index.onload = function () {
        const responseText = this.responseText
        const response = JSON.parse(responseText)


        for (const kelurahan of response.kelurahan) {
            const option = document.createElement("option")
            option.classList.add("kelurahan-option")
            option.setAttribute("value", kelurahan.nama)
            option.setAttribute("name", kelurahan.id)
            option.innerText = kelurahan.nama

            kelurahanListElement.append(option)
        }

        kelurahanListElement.addEventListener("change", function () {
            let namaKelurahan = kelurahanListElement.value;
            document.getElementById("destination-kelurahan").value = namaKelurahan
        })
    }
    index.open("GET",
        `https://dev.farizdotid.com/api/daerahindonesia/kelurahan?id_kecamatan=${kecamatanId}`
    );
    index.send();
}

const kecamatan = (kabupatenId) => {
    const index = new XMLHttpRequest();
    index.onload = function () {
        const responseText = this.responseText
        const response = JSON.parse(responseText)

        for (const kecamatan of response.kecamatan) {
            const option = document.createElement("option")
            option.classList.add("kecamatan-option")
            option.setAttribute("value", kecamatan.nama)
            option.setAttribute("name", kecamatan.id)
            option.innerText = kecamatan.nama

            kecamatanListElement.append(option)
        }

        kecamatanListElement.addEventListener("change", function () {
            resetSelectElement(kelurahanListElement)
            let namaKecamatan = kecamatanListElement.value;
            document.getElementById("destination-kecamatan").value = namaKecamatan

            for (const kecamatan of response.kecamatan) {
                if (kecamatan.nama === namaKecamatan) {
                    kelurahanListElement.removeAttribute("disabled")
                    kelurahan(kecamatan.id)
                }
            }
        })
    }
    index.open("GET",
        `https://dev.farizdotid.com/api/daerahindonesia/kecamatan?id_kota=${kabupatenId}`
    );
    index.send();
}


const kota_kabupaten = (provinsiId) => {
    const index = new XMLHttpRequest();
    index.onload = function () {
        const responseText = this.responseText
        const response = JSON.parse(responseText)

        for (const kabupaten of response.kota_kabupaten) {
            const option = document.createElement("option")
            option.classList.add("kabupaten-option")
            option.setAttribute("value", kabupaten.nama)
            option.setAttribute("name", kabupaten.id)
            option.innerText = kabupaten.nama

            kabupatenListElement.append(option)
        }

        kabupatenListElement.addEventListener("change", function () {
            resetSelectElement(kecamatanListElement)
            resetSelectElement(kelurahanListElement)

            let namaKabupaten = kabupatenListElement.value;
            document.getElementById("destination-kabupaten").value = namaKabupaten

            for (const kabupaten of response.kota_kabupaten) {
                if (kabupaten.nama === namaKabupaten) {
                    kecamatanListElement.removeAttribute("disabled")
                    kecamatan(kabupaten.id)
                }
            }
        })
    }
    index.open("GET",
        `https://dev.farizdotid.com/api/daerahindonesia/kota?id_provinsi=${provinsiId}`
    );
    index.send();
}

const provinsi = () => {
    const index = new XMLHttpRequest();
    index.onload = function () {
        const responseText = this.responseText
        const response = JSON.parse(responseText)


        for (const provinsi of response.provinsi) {
            const optionElement = document.createElement("option")
            optionElement.setAttribute("value", provinsi.nama)
            optionElement.innerText = provinsi.nama

            provinsiListElement.append(optionElement)
        }

        provinsiListElement.addEventListener("change", function () {

            resetSelectElement(kabupatenListElement)
            resetSelectElement(kecamatanListElement)
            resetSelectElement(kelurahanListElement)


            let namaProvonsi = provinsiListElement.value;
            document.getElementById("destination-provinsi").value = namaProvonsi

            kabupatenListElement.removeAttribute("disabled")
            for (const provinsi of response.provinsi) {
                if (provinsi.nama === namaProvonsi) {
                    kabupatenListElement.removeAttribute("disabled")
                    kota_kabupaten(provinsi.id)
                }
            }

        })

    }
    index.open("GET",
        "https://dev.farizdotid.com/api/daerahindonesia/provinsi"
    );
    index.send();
}

provinsi()