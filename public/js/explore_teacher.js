$(document).ready(function () {
    function teachers(mapel, lokasi, hargaFrom, hargaTo, hargaMaks) {
        $('#teachers-list').html('');
        let url = '/parent/api-teacher/';
        if ((mapel != "") && (lokasi != "")) {
            url = '/parent/api-teacher/' + mapel + '/' + lokasi;
        } else if ((mapel != "") && (lokasi == "")) {
            url = '/parent/api-teacher-mapel/' + mapel;
        } else if ((mapel == "") && (lokasi != "")) {
            url = '/parent/api-teacher-lokasi/' + lokasi;
        } else if ((hargaFrom != "") && (hargaTo != "")) {
            url = '/parent/api-teacher-harga/' + hargaFrom + '/' + hargaTo;
        } else if (hargaMaks != "") {
            url = '/parent/api-teacher-harga/' + hargaMaks;
        }
        $.ajax({
            url: url,
            type: 'get',
            dataType: 'json',
            success: function (result) {
                $.each(result.teachers, function (i, data) {
                    let output = "";
                    let hargaInt = 0;
                    let url_detail = `href="/parent/eksplorasi/${data.id}"`;
                    if (data.biaya == null) {
                        output = "belum diisi";
                        url_detail = "";
                    } else {
                        hargaInt = data.biaya;
                        let harga = hargaInt.toString(); //ubah ke string
                        let titik = ".";
                        let posisi = -3;
                        output = [harga.slice(0, posisi), titik, harga.slice(posisi)].join('');
                    }
                    let mapel = data.mata_pelajaran;
                    if (data.mata_pelajaran == null) {
                        mapel = "belum diisi";
                    }
                    let gmbr = data.avatar;
                    if (data.avatar == null || data.avatar == undefined) {
                        gmbr = '/img/square-avatar-default.jpg';
                    }

                    var sum = 0;
                    for (var i = 0; i < data.Rates.length; i++) {
                        sum += parseInt(data.Rates[i].rate, 10); //don't forget to add the base
                    }
                    var avg = sum / data.Rates.length;
                    if (isNaN(avg)) {
                        avg = 'belum ada';
                    }
                    $('#teachers-list').append(`
                        <div class="col col-lg-4 col-md-6 col-sm-12 mb-1">
                            <a ${url_detail}>
                                <div class="card teacher-list">                                
                                    <img src="${gmbr}" class="card-img-top" alt="avatar.png">
                                    <div class="card-body" id="">
                                        <h5 class="card-title">${data.nama}</h5>
                                        <p class="card-text">${mapel}</p>
                                        <p class="card-title price mb-3">${output} /jam </p>

                                        <p class="text-truncate card-text text-with-icon">
                                        <span class="ri-star-fill checked mr-2"></span><small>${avg} <span class="ml-2 mr-2">|</span> </small><i
                                                class="ri-map-pin-range-line mr-3"></i><small
                                                class="text-muted">${data.kabupaten},
                                                ${data.kecamatan}</small></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    `);
                });
            }
        });
    }
    teachers("", "", "", "", "");
    $("#search-btn").click(e => {
        const inputMapel = document.getElementById("searchMataPelajaran");
        const filterMapel = inputMapel.value.toLowerCase();
        const inputLokasi = document.getElementById("searchLokasi");
        const filterLokasi = inputLokasi.value.toLowerCase();
        teachers(filterMapel, filterLokasi, "", "", "");
    });
    $('#filter-btn').click(e => {
        const hargaFrom = $('#filter-harga').find(':selected').data('from');
        const hargaTo = $('#filter-harga').find(':selected').data('to');

        if ((hargaFrom != "") && (hargaTo != "")) {
            teachers("", "", hargaFrom, hargaTo, "");
        } else if ((hargaFrom == 0) && (hargaTo != "")) {
            teachers("", "", "0", hargaTo, "");
        } else if ((hargaFrom != "") && (hargaTo == "maks")) {
            teachers("", "", "", "", hargaFrom);
        } else if ((hargaFrom == "") && (hargaTo == "")) {
            teachers("", "", "", "", "");
        }
    });
});