$(document).ready(function () {
    function ValidateRegex(mail) {
        let regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        if (regex.test(mail)) {
            return (true)
        }
        return (false)
    }

    function validateEmail() {
        let emailValue = $('#email').val();
        if (emailValue == '') {
            $('#emailvalid').html('email masih kosong');
            return false;
        } else if (!ValidateRegex(emailValue)) {
            $('#emailvalid').html('email tidak valid');
            return false;
        } else {
            $('#emailvalid').html('');
            return true;
        }
    }

    function validatePassword() {
        let passValue = $('#password').val();
        if (passValue == '') {
            $('#passcheck').html('password masih kosong');
            return false;
        } else {
            $('#passcheck').html('');
            return true;
        }
    }

    // Submitt button
    $('#submitbtn').click(function () {
        let valid_email = validateEmail();
        let valid_pass = validatePassword();
        if ((valid_email == true) && (valid_pass == true)) {
            return true;
        } else {
            return false;
        }
    });

});