$(document).ready(function () {
    function validateAvatar() {
        let avatarValue = $('#avatar').val();
        if (avatarValue == '') {
            $('#avatarcheck').html('link foto profil masih kosong');
            return false
        } else {
            $('#avatarcheck').html('');
            return true;
        }
    }

    function validateNama() {
        let namaValue = $('#nama').val();
        if (namaValue == '') {
            $('#namacheck').html('nama masih kosong');
            return false
        } else {
            $('#namacheck').html('');
            return true;
        }
    }

    function ValidateRegex(mail) {
        let regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        if (regex.test(mail)) {
            return (true)
        }
        return (false)
    }

    function validateEmail() {
        let emailValue = $('#email').val();
        if (emailValue == '') {
            $('#emailvalid').html('email masih kosong');
            return false;
        } else if (!ValidateRegex(emailValue)) {
            $('#emailvalid').html('email tidak valid');
            return false;
        } else {
            $('#emailvalid').html('');
            return true;
        }
    }

    function validateWA() {
        let waValue = $('#no_telfon').val();
        if (waValue == '') {
            $('#wacheck').html('link nomor Whatsapp masih kosong');
            return false
        } else {
            $('#wacheck').html('');
            return true;
        }
    }

    function validateBiaya() {
        let biayaValue = $('#biaya').val();
        if (biayaValue == '') {
            $('#biayacheck').html('biaya jasa masih kosong');
            return false
        } else {
            $('#biayacheck').html('');
            return true;
        }
    }

    function validateMetodologi() {
        let metoValue = $('#metodologi').val();
        if (metoValue == '') {
            $('#metodologicheck').html('metodologi masih kosong');
            return false
        } else {
            $('#metodologicheck').html('');
            return true;
        }
    }

    function validateLatar() {
        let latarValue = $('#latar_belakang').val();
        if (latarValue == '') {
            $('#latarcheck').html('latar belakang masih kosong');
            return false
        } else {
            $('#latarcheck').html('');
            return true;
        }
    }

    function validateAlamat() {
        let alamatValue = $('#alamat').val();
        if (alamatValue == '') {
            $('#alamatcheck').html('alamat masih kosong');
            return false
        } else {
            $('#alamatcheck').html('');
            return true;
        }
    }

    function validatePendidikan() {
        let pendidikanValue = $('#pendidikan').val();
        if (pendidikanValue == '') {
            $('#pendidikancheck').html('pendidikan masih kosong');
            return false
        } else {
            $('#pendidikancheck').html('');
            return true;
        }
    }

    function validateMapel() {
        let mapelValue = $('#mata_pelajaran').val();
        if (mapelValue == '') {
            $('#mapelcheck').html('mata pelajaran masih kosong');
            return false
        } else {
            $('#mapelcheck').html('');
            return true;
        }
    }

    function validateTingkat() {
        let tingkatValue = $('#tingkat').val();
        if (tingkatValue == '') {
            $('#tingkatcheck').html('subjek yang diajarkan masih kosong');
            return false
        } else {
            $('#tingkatcheck').html('');
            return true;
        }
    }

    $("#save").click(e => {
        e.preventDefault()
        console.log(document.getElementById("id").value)
        if (document.getElementById("destination-provinsi").value == "") {
            document.getElementById("destination-provinsi").value = document.getElementById("prov_temp").value;
        }
        if (document.getElementById("destination-kabupaten").value == "") {
            document.getElementById("destination-kabupaten").value = document.getElementById("kab_temp").value;
        }
        if (document.getElementById("destination-kecamatan").value == "") {
            document.getElementById("destination-kecamatan").value = document.getElementById("kec_temp").value;
        }
        if (document.getElementById("destination-kelurahan").value == "") {
            document.getElementById("destination-kelurahan").value = document.getElementById("kel_temp").value;
        }
        let valid_avatar = validateAvatar();
        let valid_nama = validateNama();
        let valid_email = validateEmail();
        let valid_wa = validateWA();
        let valid_biaya = validateBiaya();
        let valid_metodologi = validateMetodologi();
        let valid_latar = validateLatar();
        let valid_alamat = validateAlamat();
        let valid_pendidikan = validatePendidikan();
        let valid_mapel = validateMapel();
        let valid_tingkat = validateTingkat();

        if ((valid_avatar == true) &&
            (valid_nama == true) &&
            (valid_email == true) &&
            (valid_wa == true) &&
            (valid_biaya == true) &&
            (valid_metodologi == true) &&
            (valid_latar == true) &&
            (valid_alamat == true) &&
            (valid_pendidikan == true) &&
            (valid_mapel == true) &&
            (valid_tingkat == true)) {
            $.ajax({
                url: '/teacher/' + document.getElementById("id").value,
                type: 'PUT',
                data: $('form').serialize(),
                success: res => {
                    window.location.href = '/teacher/profil';
                }
            })
        } else {
            return false;
        }
    });
});

//=====================================================================

//select elements
const provinsiListElement = document.getElementById("provinsi-list")
const kabupatenListElement = document.getElementById("kabupaten-list")
const kecamatanListElement = document.getElementById("kecamatan-list")
const kelurahanListElement = document.getElementById("kelurahan-list")

function resetSelectElement(selectElement) {
    selectElement.selectedIndex = 0; // first option is selected
    for (i = selectElement.options.length - 1; i >= 1; i--) {
        selectElement.remove(i);
    }
}

const kelurahan = (kecamatanId) => {
    const index = new XMLHttpRequest();
    index.onload = function () {
        const responseText = this.responseText
        const response = JSON.parse(responseText)


        for (const kelurahan of response.kelurahan) {
            const option = document.createElement("option")
            option.classList.add("kelurahan-option")
            option.setAttribute("value", kelurahan.nama)
            option.setAttribute("name", kelurahan.id)
            option.innerText = kelurahan.nama

            kelurahanListElement.append(option)
        }

        kelurahanListElement.addEventListener("change", function () {
            let namaKelurahan = kelurahanListElement.value;
            document.getElementById("destination-kelurahan").value = namaKelurahan
        })

    }
    index.open("GET",
        `https://dev.farizdotid.com/api/daerahindonesia/kelurahan?id_kecamatan=${kecamatanId}`
    );
    index.send();
}

const kecamatan = (kabupatenId) => {
    const index = new XMLHttpRequest();
    index.onload = function () {
        const responseText = this.responseText
        const response = JSON.parse(responseText)

        for (const kecamatan of response.kecamatan) {
            const option = document.createElement("option")
            option.classList.add("kecamatan-option")
            option.setAttribute("value", kecamatan.nama)
            option.setAttribute("name", kecamatan.id)
            option.innerText = kecamatan.nama

            kecamatanListElement.append(option)
        }

        kecamatanListElement.addEventListener("change", function () {
            resetSelectElement(kelurahanListElement)
            let namaKecamatan = kecamatanListElement.value;
            document.getElementById("destination-kecamatan").value = namaKecamatan

            for (const kecamatan of response.kecamatan) {
                if (kecamatan.nama === namaKecamatan) {
                    kelurahanListElement.removeAttribute("disabled")
                    kelurahan(kecamatan.id)
                }
            }
        })

    }
    index.open("GET",
        `https://dev.farizdotid.com/api/daerahindonesia/kecamatan?id_kota=${kabupatenId}`
    );
    index.send();
}


const kota_kabupaten = (provinsiId) => {
    const index = new XMLHttpRequest();
    index.onload = function () {
        const responseText = this.responseText
        const response = JSON.parse(responseText)

        for (const kabupaten of response.kota_kabupaten) {
            const option = document.createElement("option")
            option.classList.add("kabupaten-option")
            option.setAttribute("value", kabupaten.nama)
            option.setAttribute("name", kabupaten.id)
            option.innerText = kabupaten.nama

            kabupatenListElement.append(option)
        }

        kabupatenListElement.addEventListener("change", function () {

            resetSelectElement(kecamatanListElement)
            resetSelectElement(kelurahanListElement)


            let namaKabupaten = kabupatenListElement.value;
            document.getElementById("destination-kabupaten").value = namaKabupaten

            for (const kabupaten of response.kota_kabupaten) {
                if (kabupaten.nama === namaKabupaten) {
                    kecamatanListElement.removeAttribute("disabled")
                    kecamatan(kabupaten.id)
                }
            }
        })

    }
    index.open("GET",
        `https://dev.farizdotid.com/api/daerahindonesia/kota?id_provinsi=${provinsiId}`
    );
    index.send();
}

const provinsi = () => {
    const index = new XMLHttpRequest();
    index.onload = function () {
        const responseText = this.responseText
        const response = JSON.parse(responseText)


        for (const provinsi of response.provinsi) {
            const optionElement = document.createElement("option")
            optionElement.setAttribute("value", provinsi.nama)
            optionElement.innerText = provinsi.nama

            provinsiListElement.append(optionElement)
        }

        provinsiListElement.addEventListener("change", function () {

            resetSelectElement(kabupatenListElement)
            resetSelectElement(kecamatanListElement)
            resetSelectElement(kelurahanListElement)


            let namaProvonsi = provinsiListElement.value
            document.getElementById("destination-provinsi").value = namaProvonsi

            kabupatenListElement.removeAttribute("disabled")
            for (const provinsi of response.provinsi) {
                if (provinsi.nama === namaProvonsi) {
                    kabupatenListElement.removeAttribute("disabled")
                    kota_kabupaten(provinsi.id)
                }
            }

        })

    }
    index.open("GET",
        "https://dev.farizdotid.com/api/daerahindonesia/provinsi"
    );
    index.send();
}

provinsi()