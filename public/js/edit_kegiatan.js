$(document).ready(function () {
    function validationMapel() {
        let mapelValue = $('#mata_pelajaran').val();
        if (mapelValue == '') {
            $('#mapelcheck').html('mata pelajaran masih kosong');
            return false
        } else {
            $('#mapelcheck').html('');
            return true;
        }
    }

    function validationWaktu() {
        let waktuValue = $('#input_waktu').val();
        if (waktuValue == '') {
            $('#waktucheck').html('waktu masih kosong');
            return false
        } else {
            $('#waktucheck').html('');
            return true;
        }
    }

    function validationReport() {
        let waktuValue = $('#report_link').val();
        if (waktuValue == '') {
            $('#linkcheck').html('laporan kegiatan belajar masih kosong');
            return false
        } else {
            $('#linkcheck').html('');
            return true;
        }
    }

    $("#save-kegiatan").click(e => {
        e.preventDefault()

        let valid_mapel = validationMapel();
        let valid_waktu = validationWaktu();
        let valid_report = validationReport();

        if ((valid_mapel == true) && (valid_waktu == true) && (valid_report == true)) {
            let id = document.getElementById("task_id").value
            $.ajax({
                url: `/teacher/form_kegiatan/${id}`,
                type: 'PUT',
                data: $('form').serialize(),
                success: res => {
                    window.location.href = res
                }
            })
        } else {
            return false
        }
    })
});