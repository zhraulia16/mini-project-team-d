const {
    Order,
    Teacher,
    Parent
} = require('../models');

const STATUS = {
    PENDING: "pending",
    HIRED: "hired",
    COMPLETED: "completed",
    CANCELED: "canceled"
}

class orderController {
    //menampilkan semua order
    static createOrder(req, res) {
        const TeacherId = req.params.id
        const ParentId = req.user.id
        const status = STATUS.PENDING
        Order.create({
                TeacherId,
                ParentId,
                status,
                createdAt: new Date(),
                updatedAt: new Date()
            })
            .then((order) => {
                res.send(order)
            })
            .catch((err) => {
                console.log(err)
                // res.render("register");
            });
    }

    static allParentOrder(req, res) {
        Order.findAll({
            where: {
                ParentId: req.params.id
            },
            include: [{
                model: Teacher
            }],
            order: [
                ['id', 'DESC'],
            ]
        }).then(orders => {
            res.send(orders)
        }).catch(err => next(err))
    }

    static detailOrder(req, res) {
        const id = req.params.id
        Order.findOne({
            where: {
                ParentId: id
            }
        }).then(parent => {
            res.json({
                'status': 200,
                'data': parent
            })
        }).catch(err => {
            res.json({
                'status': 400,
                'message': err
            })
        })
    }


    static setHire(req, res) {
        const id = req.params.id
        Order.update({
            status: STATUS.HIRED
        }, {
            where: {
                id: id
            }
        }).then(order => {
            res.send('/parent/dashboard')
        }).catch(err => {
            res.json({
                'status': 400,
                'message': err
            })
        })
    }

    static setCompleted(req, res) {
        const id = req.params.id
        Order.update({
            status: STATUS.COMPLETED
        }, {
            where: {
                id: id
            }
        }).then(order => {
            res.send('/parent/dashboard')
        }).catch(err => {
            res.json({
                'status': 400,
                'message': err
            })
        })
    }

    static setCancelled(req, res) {
        const id = req.params.id
        Order.update({
            status: STATUS.CANCELED
        }, {
            where: {
                id: id
            }
        }).then(order => {
            res.send('/parent/dashboard')
        }).catch(err => {
            res.json({
                'status': 400,
                'message': err
            })
        })
    }


    static delete(req, res) {
        const id = req.params.id;
        Order.destroy({
            where: {
                id: id
            }
        }).then(() => {
            res.json({
                'status': 200,
                'message': 'success deleting'
            })
        }).catch(err => {
            res.json({
                'status': 400,
                'message': err
            })
        })
    }


    // teacher section
    
    static ParentList(req, res) {
        Order.findAll({
            where: {
                TeacherId: req.user.id
            },
            include: [{
                model: Parent
            }, {
                model: Teacher
            }],
            order: [
                ['id', 'DESC'],
            ]
        }).then(orders => {
            res.render("./teacher/form_kegiatan", {orders})
        }).catch(err => {
            res.send(err)
        })
    }

}

module.exports = orderController;