const {
    Teacher,
    Parent
} = require("../models");
const bcrypt = require("bcrypt");
var passport = require("passport");

const ROLE = {
    PARENT: 'parent',
    TEACHER: 'teacher'
}

class Controller {
    static register(req, res) {
        const obj = JSON.parse(JSON.stringify(req.body));
        const nama = obj.nama
        const email = obj.email
        const password = bcrypt.hashSync(obj.password, 10)
        const provinsi = obj.provinsi
        const kabupaten = obj.kabupaten
        const kecamatan = obj.kecamatan
        const kelurahan = obj.kelurahan
        const role = obj.role

        if (role === ROLE.TEACHER) {
            Teacher.create({
                    nama,
                    email,
                    password,
                    provinsi,
                    kabupaten,
                    kecamatan,
                    kelurahan,
                    role,
                    provinsi,
                    kabupaten,
                    kecamatan,
                    kelurahan,
                    biaya: null,
                    createdAt: new Date(),
                    updatedAt: new Date()
                })
                .then((teacher) => {
                    res.redirect("/");
                })
                .catch((err) => {
                    console.log(err)
                    res.render("register");
                });

        } else if (role === ROLE.PARENT) {
            Parent.create({
                    nama,
                    email,
                    password,
                    provinsi,
                    kabupaten,
                    kecamatan,
                    kelurahan,
                    role,
                    provinsi,
                    kabupaten,
                    kecamatan,
                    kelurahan,
                    createdAt: new Date(),
                    updatedAt: new Date()
                }).then((parent) => {
                    res.redirect("/");
                })
                .catch((err) => {
                    console.log(err)
                    res.render("register");
                });
        } else alert("role is unidentified")
    }

    static logout(req, res) {
        req.logout();
        res.redirect('/');
    }

}

module.exports = Controller