const {
    Parent,
    Teacher,
    Order,
    Task
} = require('../models');
const passport = require('../lib/passport');
// const teacher = require("./teacherController")

module.exports = {
    add: (req, res) => {
        const TeacherId = req.user.id
        const ParentId = req.body.ParentId
        const mata_pelajaran = req.body.mata_pelajaran
        const hari = req.body.input_hari
        const waktu = req.body.input_waktu
        const report_link = req.body.report_link
        Task.create({
                TeacherId,
                ParentId,
                mata_pelajaran,
                hari,
                waktu,
                report_link,
                createdAt: new Date(),
                updatedAt: new Date()
            })
            .then((task) => {
                res.redirect('/teacher/kegiatan')
            })
            .catch((err) => {
                console.log(err)
                res.send(err)
                // res.render("register");
            });
    },

    edit: (req, res) => {
        const id = req.params.id
        Task.findOne({
            where: {
                id: id
            },
            include: [{
                model: Parent
            }]
        }).then(task => {
            Order.findAll({
                where: {
                    TeacherId: req.user.id
                },
                include: [{
                    model: Parent
                }, {
                    model: Teacher
                }],
                order: [
                    ['id', 'DESC'],
                ]
            }).then(orders => {
                res.render('teacher/edit_kegiatan', {
                    task, orders
                })
            }).catch(err => {
                res.send(err)
            })
           
        }).catch(err => {
            res.json({
                'status': 400,
                'message': err
            })
        })
    },

    update: (req, res) => {
        const TeacherId = req.user.id
        const ParentId = req.body.ParentId
        const mata_pelajaran = req.body.mata_pelajaran
        const hari = req.body.input_hari
        const waktu = req.body.input_waktu
        const report_link = req.body.report_link
        Task.update({
            TeacherId,
            ParentId,
            mata_pelajaran,
            hari,
            waktu,
            report_link
        }, {
            where: {
                id: req.params.id
            }
        }).then(task => {
            res.send('/teacher/kegiatan')
        }).catch(err => {
            res.json({
                'status': 400,
                'message': err
            })
        })
    },

    delete: (req, res) => {
        // const id = req.params.id
        Task.destroy({
            where: {
                id: req.params.id
            }
        }).then(response => {            
            res.redirect('/teacher/kegiatan')
        }).catch(err => {
            res.json({
                'status': 400,
                'message': 'delete gagal'
            })
        })
    }

}