const {
    Parent,
    Teacher,
    Order,
    Rate,
    Task
} = require('../models');
const passport = require('../lib/passport');
// const teacher = require("./teacherController")
const sequelize = require("sequelize");
const Op = sequelize.Op;

module.exports = {
    register: (req, res, next) => {
        Parent.register(req.body).then(() => {
            res.redirect('login')
        }).catch(err => next(err))
    },

    login: passport.authenticate('parent', {
        successRedirect: '/parent/dashboard',
        failureRedirect: '/parent/login',
        failureFlash: true
    }),

    logout: (req, res) => {
        req.logout();
        res.redirect('/');
    },

    dashboard: (req, res) => {
        Order.findAll({
            where: {
                ParentId: req.user.id
            },
            include: [{
                model: Teacher
            }],
            order: [
                ['id', 'DESC'],
            ]
        }).then(orders => {
            res.render('dashboard_parent', {
                orders
            })
        }).catch(err => next(err))

    },


    getKegiatanPage: (req, res) => {
        Task.findAll({
            where: {
                ParentId: req.user.id
            },
            include: [{
                model: Teacher
            }],
            order: [
                ['id', 'DESC'],
            ]
        }).then(tasks => {
            res.render('./parent/kegiatan_parent', {
                tasks
            })
        })
    },

    //menampilkan semua parents
    allParent: (req, res) => {
        Parent.findAll({
            order: [
                ['id', 'ASC'],
            ],
        }).then(parents => {
            const currentUser = req.user.nama;
            res.render('dashboard_parent', {
                currentUser,
                parents
            })
        }).catch(err => next(err))
    },

    detail: (req, res) => {
        const id = req.params.id
        Parent.findOne({
            where: {
                id: id
            }
        }).then(parent => {
            res.render('parent/edit_parent', {
                parent
            })
        }).catch(err => {
            res.json({
                'status': 400,
                'message': err
            })
        })
    },

    update: (req, res) => {
        const id = req.params.id
        Parent.update({
            nama: req.body.nama,
            email: req.body.email,
            provinsi: req.body.provinsi,
            kabupaten: req.body.kabupaten,
            kecamatan: req.body.kecamatan,
            kelurahan: req.body.kelurahan,
            alamat: req.body.alamat,
        }, {
            where: {
                id: id
            }
        }).then(parent => {
            res.send('/parent/dashboard')
        }).catch(err => {
            res.json({
                'status': 400,
                'message': err
            })
        })
    },

    delete: (req, res) => {
        const id = req.params.id;
        Parent.destroy({
            where: {
                id: id
            }
        }).then(() => {
            res.json({
                'status': 200,
                'message': 'success deleting'
            })
        }).catch(err => {
            res.json({
                'status': 400,
                'message': err
            })
        })
    },

    edit: (req, res) => {
        const id = req.user.id
        Parent.findOne({
            where: {
                id: id
            }
        }).then(parent => {
            res.render('parent/edit_parent', {
                parent
            })
        }).catch(err => {
            res.json({
                'status': 400,
                'message': err
            })
        })
    },

    getEksplorePage: (req, res) => {
        Teacher.findAll({
            include: [{
                model: Rate
            }],
            order: [
                ['id', 'ASC'],
            ]
        }).then(teachers => {
            // res.send(teachers[0].Rates)
            res.render("./parent/explore_teacher", {
                teachers
            })
        }).catch(err => next(err))
    },

    getTeacherAll: (req, res) => {
        Teacher.findAll({
            order: [
                ['id', 'ASC'],
            ],
        }).then(teachers => {
            res.json({
                teachers
            })
        }).catch(err => {
            console.log(err)
        })
    },

    getTeacherDetail: (req, res) => {
        const id = req.params.id
        Teacher.findOne({
            where: {
                id: id
            }
        }).then(teacher => {
            res.render("./parent/detail_teacher", {
                teacher
            })
        }).catch(err => {
            console.log(err)
            res.json({
                'status': 400,
                'message': err
            })
        })
    },

    giveRating: (req, res) => {
        const TeacherId = req.params.id;
        Teacher.findOne({
            where: {
                id: TeacherId
            }
        }).then(teacher => {
            res.render("parent/add-rating", {
                teacher
            })
        }).catch(err => {
            res.json({
                'status': 400,
                'message': err
            })
        })
    },

    addRating: (req, res) => {
        // const TeacherId = req.params.id
        const TeacherId = req.body.teacher
        const ParentId = req.user.id
        const isFinished = true
        const rate = req.body.stars
        const comment = req.body.comment
        Rate.create({
                TeacherId,
                ParentId,
                isFinished: true,
                rate,
                comment,
                createdAt: new Date(),
                updatedAt: new Date()
            })
            .then(rate => {
                res.send('sukses');
            })
            .catch((err) => {
                console.log(err)
            });
    },

}